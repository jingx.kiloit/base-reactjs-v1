import { memo } from "react";
import { useArrayUtil } from "./modules/blog/core/action"

const Todos = ({ todos, addTodo }) => {
    const { numbersOne, numbersTwo, numbersCombined, multix } = useArrayUtil();
    return (
        <>
            <h2>My Todos</h2>
            {numbersCombined.map((todo, index) => {
                return <p key={index}>{todo}</p>;
            })}
            <button onClick={addTodo}>Add Todo</button>
        </>
    );
};

export default memo(Todos);