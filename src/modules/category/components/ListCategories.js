import Badge from "react-bootstrap/Badge";
import {useSelector} from "react-redux";


const ListCategories = ({active, setActive}) => {
    const {categories} = useSelector(state => state.category);

    return <div className="px-4">
        <div className="d-flex overflow-x-scroll w-100 pb-3">
            <Badge onClick={() => setActive("All")} className="px-4 py-2 fs-6 mx-2"
                   bg={active === "All" ? "primary" : "secondary"}>All</Badge>
            {categories.map((item, index) =>
                <Badge onClick={() => setActive(item)} key={index} className="px-4 py-2 fs-6 mx-2 "
                       bg={active === item ? "primary" : "secondary"}>{item}</Badge>)}
        </div>
    </div>
}

export default ListCategories;