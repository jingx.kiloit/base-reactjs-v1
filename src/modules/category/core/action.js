import {fetchCategories} from "./request";
import {useDispatch} from "react-redux";
import {setCategories} from "./reducer";


const useCategory = () => {
    const dispatch = useDispatch();
    const getCategories = (limit) => {
        fetchCategories(limit).then((response) => {
            dispatch(setCategories(response.data));
        });
    }

    return {getCategories}
}

export {useCategory}

