import {setProduct, setTotal} from "./reducer";
import {useDispatch} from "react-redux";
import {fetchListProduct, fetchListProductByCategory, fetchSearchProduct} from "./request";


const useProduct = () => {
    const dispatch = useDispatch();
    const getProducts = (limit) => {
        fetchListProduct(limit).then((response) => {
            dispatch(setProduct(response.data.products));
            dispatch(setTotal(response.data.total));
        });
    }

    const getProductsByCate = (limit, active) => {
        fetchListProductByCategory(limit, active).then((response) => {
            dispatch(setProduct(response.data.products));
            dispatch(setTotal(response.data.total));
        });
    }

    const searchProducts = (text) => {
        fetchSearchProduct(text).then((response) => {
            dispatch(setProduct(response.data.products));
            dispatch(setTotal(response.data.total));
        });
    }

    return {getProducts, getProductsByCate, searchProducts}
}

export {useProduct}

