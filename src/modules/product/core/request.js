import axios from "axios";



const fetchListProduct = (limit = 10, skip = 0) => {
    return axios.get("/products", {
        params: { limit, skip }
    });
}
const fetchListProductByCategory = (limit = 10, path = "") => {
    return axios.get(`/products/category/${path}`, {
        params: { limit }
    });
}

const fetchProductById = (id) => {
    return axios.get(`/products/${id}`);
}

const fetchSearchProduct = (params) => {
    return axios.get("/products/search", {
        params: params
    });
}

const fetchCategories = () => {
    return axios.get("/products/categories");
}


export { fetchSearchProduct, fetchListProductByCategory, fetchCategories, fetchListProduct, fetchProductById }