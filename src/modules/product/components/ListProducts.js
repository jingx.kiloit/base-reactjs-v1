import {useEffect, useState} from "react";
import {useNavigate} from "react-router-dom";
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';

import PaginationBasic from "./Pagination";
import {useProduct} from "../core/action";
import {useSelector} from "react-redux";
import ListCategories from "../../category/components/ListCategories";
import {useCategory} from "../../category/core/action";


const ListProducts = () => {

    const [active, setActive] = useState("All");
    const [searchText, setSearchText] = useState("");
    const navigate = useNavigate();
    const limit = 8;
    const {getProducts, getProductsByCate, searchProducts} = useProduct();
    const {getCategories} = useCategory();
    const {products, total} = useSelector(state => state.product);


    useEffect(() => {
        getProducts(limit);
        getCategories();
        // eslint-disable-next-line
    }, []);


    useEffect(() => {
        active === "All" ?
            getProducts(limit)
            : getProductsByCate(limit, active)
        // eslint-disable-next-line
    }, [active]);


    const onSearch = () => {
        searchProducts({q: searchText});
    }


    return <div className="container mt-5">
        <div className="w-100 d-flex justify-content-center mb-4">
            <Form className="d-flex justify-content-center px-5 w-50">
                <Form.Control
                    type="search"
                    placeholder="Search"
                    className="me-2"
                    aria-label="Search"
                    onChange={(e) => setSearchText(e.target.value)}
                />
                <Button onClick={onSearch} variant="outline-success">Search</Button>
            </Form>
        </div>

        <ListCategories active={active} setActive={setActive}/>


        <div className="d-flex flex-wrap justify-content-center">
            {products.length === 0 && <div>
                Not Found
            </div>}
            {products.map((product, index) => {
                return <div onClick={() => navigate(`product/${product.id}`)} className="mx-2 border rounded-2 my-2"
                            style={{}} key={index}>
                    <img style={{
                        height: "200px",
                        width: "300px"
                    }} className="" src={product.thumbnail} alt="__"/>
                    <p className="border-top px-2">{product.title}</p>
                </div>
            })}
        </div>
        <div className="d-flex justify-content-center my-3">
            <PaginationBasic onChangePage={(page) => {
                getProducts(limit, (page - 1) * limit);
            }} total={total / limit}/>
        </div>
    </div>
}

export default ListProducts;